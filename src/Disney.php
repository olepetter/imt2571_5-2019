<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();
    //gets list of all actors
        $actors = $this->xpath->query('Actors/Actor');
        foreach ($actors as $actor) {					//scans all of actors
			  $actorChilds = $actor->childNodes;
			  $actorID = $actor->getAttribute('id');	//fetches the actor-id
			  $actorName = $actorChilds->item(1)->nodeValue; //fetches the actorname
			  $arrayName = array();						//creates a new array for each actor =actorID
			  $movies = $this->xpath->query("Subsidiaries/Subsidiary
			  /Movie[Cast/Role[@actor='$actorID']]");	//finds the movies which actors has a part
	
	
			foreach ($movies as $movie) {
				$movieChilds = $movie->childNodes;					//gets the childs of each movie
				$movieTitle = $movieChilds->item(1)->nodeValue;		//gets the title
				$movieYear = $movieChilds->item(3)->nodeValue;		//gets the year
	            $nodeRole = $this->xpath->query("Subsidiaries/Subsidiary/Movie[Name='$movieTitle']
				/Cast/Role[@actor='$actorID']/@name");	//finds the node where the actor and movie exists
    
	
            $movieRole = $nodeRole[0]->nodeValue; //retrives the role
            $arrayName[] = "As {$movieRole} in {$movieTitle} ({$movieYear})"; //adds to the array of the actor
          }
          $result[$actorName] = $arrayName; //adds the array to the result-array
        }
          return $result; //returns the result
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
        //To do:
        // Implement functionality as specified
		$actors = $this->xpath->query('Actors/Actor');	//finds every actor's
			foreach ($actors as $actor) {
			$actorID = $actor->getAttribute('id');		//get's the id of the actor's
			$movies = $this->xpath->query("Subsidiaries/Subsidiary/
				Movie[Cast/Role[@actor='$actorID']]");		//finds a list of movies that the actor has a role
          			if($movies->length == 0){					//if actor dont have a role
					$actor->parentNode->removeChild($actor);	//remove's the child ($actor) from it's parentnode
        }
      }

    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        //To do:
        // Implement functionality as specified

		$castList = $this->xpath->query("Subsidiaries/Subsidiary[@id='$subsidiaryId']
		/Movie[Name='$movieName' and Year='$movieYear']/Cast")[0];	//retrives a list of actor/cast in the movies
			$role = $this->doc->createElement("Role");		//creates the element for role 
			$role->setAttribute('name', $roleName);			//set's the attribute rolename
			$role->setAttribute('actor', $roleActor);		//set's the actorname
        
			$castList->appendChild($role);				
    }
}
?>
